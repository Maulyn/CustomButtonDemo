//
//  ViewController.h
//  CustomButton
//
//  Created by maulyn on 2018/5/29.
//  Copyright © 2018年 TGCASCE. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SWSTAnswerButton.h"

@interface ViewController : NSViewController

@property (weak) IBOutlet NSButton *canSelectCheck;
@property (weak) IBOutlet NSButton *hasBorderCheck;

@property (weak) IBOutlet SWSTAnswerButton *colorButton;
@property (weak) IBOutlet SWSTAnswerButton *imageButton;

@end

