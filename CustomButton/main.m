//
//  main.m
//  CustomButton
//
//  Created by maulyn on 2018/5/29.
//  Copyright © 2018年 TGCASCE. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
