//
//  ViewController.m
//  CustomButton
//
//  Created by maulyn on 2018/5/29.
//  Copyright © 2018年 TGCASCE. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.colorButton.canSelected = self.canSelectCheck.state;
    self.colorButton.hasBorder = self.hasBorderCheck.state;
    self.imageButton.canSelected = self.canSelectCheck.state;
    self.imageButton.hasBorder = self.hasBorderCheck.state;
}


- (IBAction)canSelect:(id)sender {
    
    self.colorButton.canSelected = self.canSelectCheck.state;
    self.imageButton.canSelected = self.canSelectCheck.state;
    self.colorButton.buttonState = SWSTAnswerButtonNormalState;
    self.imageButton.buttonState = SWSTAnswerButtonNormalState;
}

- (IBAction)hasBorder:(id)sender {
    
    self.colorButton.hasBorder = self.hasBorderCheck.state;
    self.imageButton.hasBorder = self.hasBorderCheck.state;
}

@end
